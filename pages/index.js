import Layout from '../components/Layout';
import Link from 'next/Link';
import Image from 'next/image';

export default function Home({ pokemon }) {
  console.log(pokemon);
  return (
    <>
      <Layout title="vrund's pokedex">
        <h1>Vrund Pokedex</h1>
        <ul>
          {pokemon.map((pokemon, index) => (
            <li key={index}>
              <Link href={`/pokemon?id=${index + 1}`}>
                <a>
                  <Image
                    src={pokemon.image}
                    alt={pokemon.name}
                    width={80}
                    height={80}
                  />
                  {pokemon.name}
                </a>
              </Link>
            </li>
          ))}
        </ul>
      </Layout>
    </>
  );
}

export async function getStaticProps() {
  try {
    const res = await fetch('https://pokeapi.co/api/v2/pokemon?limit=9');
    const { results } = await res.json();
    const pokemon = results.map((result, index) => {
      const newIndex = ('00' + (index + 1)).slice(-3);
      const image = `https://assets.pokemon.com/assets/cms2/img/pokedex/detail/${newIndex}.png`;
      return {
        ...result,
        image,
      };
    });
    return {
      props: { pokemon },
    };
  } catch (err) {
    console.error(err);
  }
}
