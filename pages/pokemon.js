import React from 'react';
import Layout from '../components/Layout';
import Link from 'next/Link';
import Image from 'next/Image';

export default function pokemon({ pokemon }) {
  return (
    <Layout title={pokemon.name}>
      <h1>{pokemon.name}</h1>
      <Image src={pokemon.image} alt={pokemon.name} width={100} height={100} />
      <h6>Weight:</h6>
      <span>{pokemon.weight}</span>
      <h6>Height:</h6>
      <span>{pokemon.height}</span>
    </Layout>
  );
}

export async function getServerSideProps({ query }) {
  const id = query.id;
  try {
    const res = await fetch(`https://pokeapi.co/api/v2/pokemon/${id}`);
    const pokemon = await res.json();
    const newIndex = ('00' + id).slice(-3);
    const image = `https://assets.pokemon.com/assets/cms2/img/pokedex/detail/${newIndex}.png`;
    pokemon.image = image;

    return {
      props: {
        pokemon,
      },
    };
  } catch (err) {
    console.error(err);
  }
}
